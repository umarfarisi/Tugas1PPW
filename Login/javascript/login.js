$(document).ready(function(){

	var username = [];
	var password = [];
	var urlJson = "http://localhost/tugas1/login/json/users.json";
	var userNameKey;
	var usernameSuccess;
	
	$.ajax({
		url: urlJson,
		beforeSend: function(data){
		    if (data.overrideMimeType)
		    {
		      data.overrideMimeType("application/json");
		    }
		},
		dataType: 'json',
		success: function(result){
			var users = result.users;
			for(i = 0 ; i < users.length ; i++){
				var currentUser = users[i];
				var currentUsername = currentUser.username;
				var currentPassword = currentUser.password;
				username.push(currentUsername);
				password.push(currentPassword);
			}
		}
	});

	$("#loginButton").click(function(){
		var usernameOfUser = $("#username").val();
		var passwordOfUser = $("#password").val();
		var indexOfUsername = username.indexOf(usernameOfUser);
		var indexOfPassword = password.indexOf(passwordOfUser);
		var isValid = usernameOfUser.length !== 0 && passwordOfUser !==0;
		if(isValid){
			var isLoginSuccess = indexOfUsername !== -1 && indexOfPassword !== -1;
			if(isLoginSuccess){
				loginSuccess();
			}else{
				alert("username or password is wrong, please try again");
			}
		}
	});

	function loginSuccess(){
		usernameSuccess = $("#username").val();
		sessionStorage.name = usernameSuccess;
		window.open("../Game/game.html","_self");
	}

});