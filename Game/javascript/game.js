$(document).ready(function(){

	var imgsDir = "imgs/";
	var dafaultImgs = "default_image";
	var imgs = ["0","1","2","3","4","5","6","7","0","1","2","3","4","5","6","7"];
	var extension = ".png";
	var imgsContainerSize = 16;
	var countImgsOpen = 0;
	var prefImg;
	var prefImgIdOne = "init id";
	var prefImgIdTwo = "init id";
	var solvedImgId = [];
	var isStart = false;
	var second = 0;
	var minute = 0;
	var hour = 0;
	var timeLoad;

	makeRandomArray();
	loadImgs();
	loadName();

	function loadName(){
		$("#username").html(sessionStorage.name);
	}

	function loadTime(){
		timeLoad = setInterval(function(){
			second++;
			if(second === 60){
				minute++;
				second = 0;
			}
			if(minute === 60){
				hour++;
				minute = 0;
			}
			var hourDisplay = hour;
			var minuteDisplay = minute;
			var secondDisplay = second;

			if(hour < 10){
				hourDisplay = "0"+hourDisplay;
			}
			if(minute < 10){
				minuteDisplay = "0"+minuteDisplay;
			}
			if(second < 10){
				secondDisplay = "0"+secondDisplay;
			}
			$("#time").html(hourDisplay+":"+minuteDisplay+":"+secondDisplay);
		},1000);
	}

	$(".imageContainer").click(function(){
		var currentId = $(this).attr("id");
		if(isStart && solvedImgId.indexOf(currentId) == -1){
			$(this).find("img").animate({
				height: '120px',
	            width: '120px'
			}, function(){
				if(prefImgIdOne !== null && currentId !== prefImgIdOne){
					countImgsOpen += 1;
					if(countImgsOpen == 2){
						prefImgIdTwo = currentId;
						if(prefImg === imgs[currentId]){
							imgsMatch();
						}else{
							imgsDontMatch();
						}
						countImgsOpen = 0;
					}else{
						prefImgIdOne = currentId;
						prefImg = imgs[currentId];
					}
				}
			});
		}

	});

	$("#startButton").click(function(){
		prepareToStart();
		isStart = true;
		loadTime();
	});

	function prepareToStart(){
		for(i = 0 ; i < solvedImgId.length ; i++){
			$("#"+solvedImgId[i]).find("img").animate({
				height: '0px',
	            width: '120px'
			});
		}
		clearInterval(timeLoad);
		second = 0;
		minute = 0;
		hour = 0;
		solvedImgId = [];
	}

	function imgsMatch(){
		solvedImgId.push(prefImgIdOne);
		solvedImgId.push(prefImgIdTwo);
		prefImgIdOne = "dari ulang";
		prefImgIdTwo = "dari ulang";
		imgsContainerSize -= 2;
		if(imgsContainerSize === 0){
			gameFinish();
		}
	}

	function gameFinish(){
		clearInterval(timeLoad);
		var totalWaktu = hour*60*60+minute*60+second;
		var username = $("#username").html();
	}

	function imgsDontMatch(){
		
		$("#"+prefImgIdOne).find("img").animate({
			height: '0px',
            width: '120px'
		});
		$("#"+prefImgIdTwo).find("img").animate({
			height: '120px',
            width: '0px'
		});
	}

	function getImg(img){
		return imgsDir+img+extension;
	}


	function makeRandomArray(){
		var randomIndex;
		var temptArray = [];
		while(imgs.length > 0){
			randomIndex = Math.floor(Math.random() * imgs.length);
			temptArray.push(imgs[randomIndex]);
			imgs.splice(randomIndex, 1);
		}
		imgs = temptArray;
	}

	function loadImgs(){
		for(i = 0 ; i < imgsContainerSize ; i++){
			var currentElementID = "#"+i;
			$(currentElementID+" img").attr("src",getImg(imgs[i]));
		}
	}

	function loadBestScore(){
		for(i = 0 ; i < 5 ; i++){
			var currentItem = localStorage.getItem(i).split("###");
		}
	}

});
